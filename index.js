const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
let users = [{
		"username": "username1",
		"password": "password1"
	},
	{
		"username": "username2",
		"password": "password2"
	},
	{
		"username": "username3",
		"password": "password3"
	}
];

app.use(express.json());

app.get("/home", (request, response) =>{
	response.send("Welcome to homepage!");
});


app.get("/users", (request, response) =>{
	response.send(users);
});


app.delete("/delete-user", (request, response) =>{
	let username = request.body.username;
	let index = users.map(user => user.username)
		.indexOf(username);

	if(index >= 0 ){
		users.splice(index, 1);
		response.send(`Deleted ${username} from the record.`);
	} else {
		response.send(`Cannot find ${username} from the record. ${index}`);
	};

});


app.listen(port, () => console.log(`Server is running on localhost:${port}`));



